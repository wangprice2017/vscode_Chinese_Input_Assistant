### 发布方法

1. 本地打包

由于插件商店图片路径默认为支持 github，需要另行指定：

```
vsce package --baseImagesUrl https://gitee.com/Program-in-Chinese/vscode_Chinese_Input_Assistant/raw/master/
```

2. 发布 .vsix 包

```
vsce publish -p 【token】 --packagePath ChineseInputAssistant-x.y.z.vsix
```

### 测试

在调试界面下运行“Extension Tests”。
