
## JavaScript

当中文输入状态时，补全项中有时会同时存在带拼音项与不带拼音项，尤其是输入比较慢时更明显。比如输入“中”的过程中，vsc 会在每个字符输入时返回建议（如下）。也许由于每次返回的延时，最后的补全项会有不同。
```
"输入词" "z"
日志.js:3
"返回对象" ["中文变量\tZhongWenBianLiang","加法\tJiaFa","小类\tXiaoLei","零蛋\tLingDan","使用\tShiYong","语法需要\tYuFaXuYao","或者\tHuoZhe","输入\tShuRu"]
日志.js:3
"================"
日志.js:3
"输入词" "zhon"
日志.js:3
"返回对象" ["中文变量\tZhongWenBianLiang","加法\tJiaFa","小类\tXiaoLei","零蛋\tLingDan","使用\tShiYong","语法需要\tYuFaXuYao","或者\tHuoZhe","输入\tShuRu"]
日志.js:3
"================"
日志.js:3
"输入词" "zhong"
日志.js:3
"返回对象" ["中文变量\tZhongWenBianLiang","加法\tJiaFa","小类\tXiaoLei","零蛋\tLingDan","使用\tShiYong","语法需要\tYuFaXuYao","或者\tHuoZhe","输入\tShuRu"]
日志.js:3
"================"
日志.js:3
"输入词" "中"
日志.js:3
"返回对象" ["中文变量\tZhongWenBianLiang","加法\tJiaFa","小类\tXiaoLei","零蛋\tLingDan","中文\tZhongWen","另一个中文\tLingYiGeZhongWen","使用\tShiYong","语法需要\tYuFaXuYao","或者\tHuoZhe","输入\tShuRu"]
```
## 代码片段

输入某些中文，比如 JS 的“出现”时，需要 ctrl+space 触发补全，而且会出现两个同名选项。此时只有选择第一项才有实际代码内容。
