exports.转换为大写 = 字符串 => 字符串.toLocaleUpperCase()
exports.包含中文 = function 包含中文(str) {
    return /[\u4e00-\u9fa5\u3007]/.test(str)
}
exports.是纯字母 = function 是纯字母(str) {
    return /[A-Za-z]/.test(str)
}
exports.查找字段 = function 查找字段(s) {
    var wordPattern = /(-?\d*\.\d\w*)|([^\`\~\!\@\^\&\*\(\)\-\#\?\=\+\[\{\]\}\\\|\;\:\'\"\,\.\<\>\/\s？。，、；：？…—·ˉˇ¨“”～〃｜《》〔〕（），]+)/g;
    return Array.from(new Set(s.match(wordPattern)))
}
